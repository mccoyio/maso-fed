var gulp = require('gulp'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    uglify = require('gulp-uglify'),
    newer = require('gulp-newer'),
    globbing = require('gulp-css-globbing'),
    cmq = require('gulp-combine-mq'),
    server = require('gulp-server-livereload'),
    imagemin = require('gulp-imagemin');


// ROOT TASKS // ---------------------------------------------------------
// Main style task
gulp.task('css', function() {
  return gulp.src('dev/sass/application.scss')
    .pipe(globbing({extensions: '.scss'}))
    .pipe(sass())
    .on('error', handleError)
    .pipe(cmq()) // combine all @media queries into the page base
    .pipe(autoprefixer({cascade: false})) // auto prefix
    .pipe(gulp.dest('../maso-bed/wp-content/themes/mason-plumlee/assets/css'))
    .pipe(gulp.dest('public/css'));
});

// Main Javascript task
gulp.task('js', function() {
  return gulp.src('dev/js/**/*.js')
    .pipe(newer('public/js'))
    .pipe(uglify())
    .on('error', handleError)
    .pipe(gulp.dest('../maso-bed/wp-content/themes/mason-plumlee/assets/js'))
    .pipe(gulp.dest('public/js'));
});

gulp.task('img', function() {
  return gulp.src('dev/img/**/*.{jpg,jpeg,png,gif,svg,ico}')
    .pipe(newer('public/img'))
    .pipe(imagemin({
      optimizationLevel: 5,
      progressive: true,
      interlaced: true,
      svgoPlugins: [{
        collapseGroups: false,
        removeViewBox: false
      }]
    }))
    .on('error', handleError)
    .pipe(gulp.dest('public/img'));
});



// FUNCTIONS // ---------------------------------------------------------
// Initial start function
gulp.task('start', ['img'], function() {
  gulp.start('js', 'css');
});

// Watch function
gulp.task('watch', ['start'], function() {
  gulp.watch('dev/sass/**/*.scss', ['css']);
  gulp.watch('dev/js/**/*.js', ['js']);
  gulp.watch('dev/img/*.{jpg,jpeg,png,gif,svg,ico}', ['img']);
  gulp.src('public/')
    .pipe(server({
      // If you'd like to utilize an IP based local server for mobile testing:
      // 1. Find your own local IP address.
      // 2. Uncomment, and set the host field below to match that.
      // 3. Open up on your mobile device, or anyother device connected to the same wifi network your gulp file is being run on
      // 4. Enter your IP Address, along with the port number you see in the terminal as the address.
      // 5. Have fun and make cool shit!
      // host: '0.0.0.0',
      livereload: true,
      directoryListing: false,
      open: true,
      defaultFile: "index.html"
    }));
});

// Default function
gulp.task('default', ['watch']);

// Error reporting function
function handleError(err) {
  console.log(err.toString());
  this.emit('end');
}
