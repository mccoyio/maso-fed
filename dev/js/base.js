(function($) {

  $(window).scroll(function() {

    var scroll_top = $(window).scrollTop();

    if( scroll_top >= 60 ) {
      $('header.global').addClass('scrolled');
    } else {
      $('header.global').removeClass('scrolled');
    }

  });

  $('.mobile-menu-trigger').on('click', function(e) {
    e.preventDefault();

    $('header.global').toggleClass('open');

    $(this).toggleClass('open');

    $('nav.mobile-menu').toggleClass('open');

    setTimeout(function() {
      $('nav.mobile-menu').toggleClass('show');
    }, 200);

  });

  $('.scroll-down').on('click', function(e) {
    e.preventDefault();

    var header = $('header');

    header.addClass('scrolled');

    var hero_height = $('.hero').height(),
      header_height = header.height(),
      scroll_distance = hero_height - header_height;


    $('html,body').animate({
      scrollTop: scroll_distance
    }, 1000);

  });

  $('.banner.slider').slick({
    dots: true,
    infinite: true,
    arrows: false,
    slidesToShow: 1,
    slidesToScroll: 1
  });

})(jQuery);
